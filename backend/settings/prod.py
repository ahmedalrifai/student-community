""" Production Settings """

import os
import dj_database_url
from .dev import *
from .utils import to_bool


############
# DATABASE #
############
DATABASES = {
    'default': dj_database_url.config(
        default=os.getenv('DATABASE_URL')
    )
}


############
# SECURITY #
############

SECRET_KEY = os.getenv('SECRET_KEY')
DEBUG = to_bool(os.environ.get('DEBUG', False))
# Set to your Domain here (eg. 'django-vue-template-demo.herokuapp.com')
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(';')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
