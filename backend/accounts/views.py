from django.shortcuts import render
from django.contrib.auth.models import User

from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated

from backend.api.serializers import PostSerializer
from backend.api.models import Post
from . import serializers


class ProfileView(RetrieveUpdateAPIView):
    serializer_class = serializers.ProfileSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user.profile


class AuthenticatedUserDetailView(RetrieveUpdateAPIView):
    serializer_class = serializers.UserDetailsSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user

class UserDetailView(RetrieveUpdateAPIView):
    serializer_class = serializers.UserDetailsSerializer
    queryset = User.objects.all()
    lookup_field = 'username'
    
    """def get_queryset(self):
        print(self.request.data)"""

class UserList(ListAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserDetailsSerializer


class UserPostsList(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        username = self.kwargs.get('username')
        return Post.objects.filter(user__username=username)
