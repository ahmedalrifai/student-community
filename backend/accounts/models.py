from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.modelfields import PhoneNumberField

from backend.api import models as api_models


class Profile(models.Model):
    user = models.OneToOneField(User, verbose_name=_('User'),
                                on_delete=models.CASCADE, blank=True, null=True)
    university = models.ForeignKey(api_models.University, related_name="profiles", 
     verbose_name=_("University"), on_delete=models.CASCADE, blank=True, null=True)
    college = models.ForeignKey(api_models.College, related_name="profiles",
     verbose_name=_("College"), on_delete=models.CASCADE, blank=True, null=True)
    department = models.ForeignKey(api_models.Department, related_name="profiles",
     verbose_name=_("Department"), on_delete=models.CASCADE, blank=True, null=True)
    student_number = models.IntegerField(verbose_name=_("Student Number"),
                                         blank=True, null=True)
    phone_number = PhoneNumberField(verbose_name=_('Phone Number'), blank=True, null=True)
    age = models.DateField(verbose_name=_("Age"), blank=True, null=True)
    city = models.CharField(verbose_name=_("City"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')
    
    def __str__(self):
        return f'{self.student_number} ({self.user.username})'
