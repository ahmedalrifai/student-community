from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import SerializerMethodField

from allauth.account.adapter import get_adapter
from allauth.account.models import EmailAddress
from allauth.utils import email_address_exists

from avatar.models import Avatar
from avatar.signals import avatar_updated

from backend.accounts.models import api_models
from backend.api import models as api_models
from backend.api.utils import u_get_avatar_url, u_validate_avatar

from . import models


class ProfileSerializer(serializers.ModelSerializer):
    university_info = SerializerMethodField()
    college_info = SerializerMethodField()
    department_info = SerializerMethodField()

    class Meta:
        model = models.Profile
        fields = ('id', 'university', 'university_info', 'college', 'college_info', 'department', 'department_info',
            'student_number', 'phone_number', 'age', 'city')
    
    def get_university_info(self, obj):
        if obj.university is not None:
            slug = obj.university.slug
            name = obj.university.name

            return {
                'slug': slug,
                'name': name
            }
    
    def get_college_info(self, obj):
        if obj.college is not None:
            slug = obj.college.slug
            name = obj.college.name

            return {
                'slug': slug,
                'name': name
            }
    
    def get_department_info(self, obj):
        if obj.department is not None:
            slug = obj.department.slug
            name = obj.department.name

            return {
                'slug': slug,
                'name': name,
            }
    
    def get_name(self, obj):
        return obj.user.get_full_name()
    
    def get_username(self, obj):
        return obj.user.username
    
    def get_email(self, obj):
        return obj.user.email
    
    def validate_university(self, university):
        if not university:
            raise serializers.ValidationError(_('University can\'t be emptey'))
        
        return university

    def validate_college(self, college):
        if not college:
            raise serializers.ValidationError(_('College can\'t be emptey'))
        
        if self.instance:
            instance_university = self.instance.university
        else:
            request = self.context.get("request")
            university = request.data.get("profile.university")

            instance_college = request.data.get("profile.college")
            if instance_college is None and college is not None:
                return college
            
            instance_university = api_models.University.objects.get(id=university)

        if college.university != instance_university:
            raise serializers.ValidationError(_('You must enter a college that is in') + f' {instance_university.name}.')
        
        return college
    
    def validate_department(self, department):
        if not department:
            raise serializers.ValidationError(_('Department can\'t be emptey'))
        
        try:
            if self.instance:
                instance_college = self.instance.college
            else:
                request = self.context.get("request")
                college = request.data.get("profile.college")

                instance_department = request.data.get("profile.department")
                if instance_department is None and department is not None:
                    return department
                
                instance_college = api_models.College.objects.get(id=college)

            if department.college != instance_college:
                raise serializers.ValidationError(_('You must enter a department that is in') + f' {instance_college.name}.')
        except ValueError:
            pass
        
        return department


class UserDetailsSerializer(serializers.ModelSerializer):
    avatar_url = serializers.SerializerMethodField()
    profile = ProfileSerializer()
    avatar = serializers.ImageField(write_only=True, required=False)
    email_status = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'email_status', 'profile', 'avatar', 'avatar_url')
    
    def get_email_status(self, obj):
        email_address = EmailAddress.objects.get(user=obj)
        return email_address.verified

    def get_avatar_url(self, obj, size=settings.AVATAR_DEFAULT_SIZE):
        return u_get_avatar_url(obj, size)

    def validate_avatar(self, avatar):
        return u_validate_avatar(avatar)
    
    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if email and email_address_exists(email, exclude_user=self.context.get('request').user):
            raise serializers.ValidationError(
                _('A user is already registered with this e-mail address.'))
        return email

    def _update_profile(self, instance, profile):
        new_university = profile.get('university')
        university = instance.profile.university
        instance.profile.university = new_university if new_university != university else university

        new_college = profile.get('college')
        college = instance.profile.college
        instance.profile.college = new_college if new_college != college else college

        new_department = profile.get('department')
        department = instance.profile.department
        instance.profile.department = new_department if new_department != department else department
        
        new_city = profile.get('city')
        city = instance.profile.city
        instance.profile.city = new_city if new_city != city else city

        new_age = profile.get('age')
        age = instance.profile.age
        instance.profile.age = new_age if new_age != age else age

        new_phone_number = profile.get('phone_number')
        phone_number = instance.profile.phone_number
        instance.profile.phone_number = new_phone_number if new_phone_number != phone_number else phone_number

        new_student_number = profile.get('student_number')
        student_number = instance.profile.student_number
        instance.profile.student_number = new_student_number if new_student_number != student_number else student_number
            
    def _update_email(self, instance, request, email):
        adapter = get_adapter()
        adapter.send_mail('account/email/email_change', instance.email, {})
        email_address = EmailAddress.objects.get(
            user=instance, verified=True)
        email_address.change(request, email, True)
        instance.email = email

    def _update_avatar(self, instance, request):
        avatar = Avatar(user=instance, primary=True)
        image_file = request.FILES['avatar']
        avatar.avatar.save(image_file.name, image_file)
        avatar.save()
        avatar_updated.send(sender=Avatar, user=instance, avatar=avatar)

    def update(self, instance, validated_data):
        request = self.context.get('request')
        profile = validated_data.get('profile', None)

        # Update user
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.first_name)

        # Update profile
        if profile:
            self._update_profile(instance, profile)

        # Update avatar
        if 'avatar' in request.FILES:
            self._update_avatar(instance, request)

        instance.save()
        return instance
