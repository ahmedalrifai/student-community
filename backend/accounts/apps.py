from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'backend.accounts'

    def ready(self):
        import backend.accounts.signals 
