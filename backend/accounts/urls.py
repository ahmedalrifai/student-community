from django.urls import path

from . import views


urlpatterns = [
    path('profile', views.ProfileView.as_view(), name='profile'),
    path('user', views.AuthenticatedUserDetailView.as_view(), name='auth_user-detail'),
    path('users', views.UserList.as_view(), name='user_list'),
    path('users/<username>/', views.UserDetailView.as_view(), name='user-detail'),
    path('users/<username>/posts', views.UserPostsList.as_view(), name='user_posts_list')
]
