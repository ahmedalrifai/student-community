"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from rest_framework_nested import routers

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from .api import views


router = routers.SimpleRouter()
router.register(r'messages', views.MessageViewSet)
router.register(r'universities', views.UniversityViewSet)

universities_router = routers.NestedSimpleRouter(router, r'universities', lookup='university')
universities_router.register(r'colleges', views.CollegeViewSet, basename='college')
universities_router.register(r'posts', views.PostViewSet, basename='post')


colleges_router = routers.NestedSimpleRouter(universities_router, r'colleges', lookup='college')
colleges_router.register(r'departments', views.DepartmentViewSet, basename='department')
colleges_router.register(r'posts', views.PostViewSet, basename='post')


departments_router = routers.NestedSimpleRouter(colleges_router, r'departments', lookup='department')
departments_router.register(r'posts', views.PostViewSet, basename='departmet_post')
departments_router.register(r'posts', views.PostViewSet, basename='post')


u_posts_router = routers.NestedSimpleRouter(universities_router, r'posts', lookup='post')
c_posts_router = routers.NestedSimpleRouter(colleges_router, r'posts', lookup='post')
d_posts_router = routers.NestedSimpleRouter(departments_router, r'posts', lookup='post')

u_posts_router.register(r'comments', views.CommentViewSet, basename='comment')
d_posts_router.register(r'comments', views.CommentViewSet, basename='comment')
c_posts_router.register(r'comments', views.CommentViewSet, basename='comment')

u_comments_router = routers.NestedSimpleRouter(u_posts_router, r'comments', lookup='comment')
c_comments_router = routers.NestedSimpleRouter(c_posts_router, r'comments', lookup='comment')
d_comments_router = routers.NestedSimpleRouter(d_posts_router, r'comments', lookup='comment')

u_comments_router.register(r'replies', views.ReplyViewSet, basename='reply')
c_comments_router.register(r'replies', views.ReplyViewSet, basename='reply')
d_comments_router.register(r'replies', views.ReplyViewSet, basename='reply')


urlpatterns = [

    # http://localhost:8000/
    path('', views.index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('', include('backend.avatars.urls')),
    path('api/news_feed', views.NewsFeedListView.as_view()),
    path('api/', include(router.urls)),
    path('api/', include(universities_router.urls)),
    path('api/', include(colleges_router.urls)),
    path('api/', include(departments_router.urls)),
    path('api/', include(u_posts_router.urls)),
    path('api/', include(c_posts_router.urls)),
    path('api/', include(d_posts_router.urls)),

    path('api/', include(u_comments_router.urls)),
    path('api/', include(c_comments_router.urls)),
    path('api/', include(d_comments_router.urls)),

    # http://localhost:8000/api/auth/[regestration]
    path('api/', include('backend.accounts.urls')),
    path('api/auth/', include('rest_auth.urls')),
    path('api/auth/obtain_token/', obtain_jwt_token),
    path('api/auth/refresh_token/', refresh_jwt_token),
    re_path(r'^api/auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$', TemplateView.as_view(template_name='account_confirm_email.html'),
        name='account_confirm_email'),
    path('api/auth/registration/', include('rest_auth.registration.urls')),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
