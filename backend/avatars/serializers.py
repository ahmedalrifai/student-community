import os

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from django.conf import settings
from django.utils.module_loading import import_string

from avatar.models import Avatar
from avatar.signals import avatar_updated

from backend.api.utils import u_get_avatar_url, u_validate_avatar


class UploadAvatarSerializer(serializers.ModelSerializer):
    avatar_url = serializers.SerializerMethodField()

    class Meta:
        model = Avatar
        fields = ('avatar', 'avatar_url',)

    def get_avatar_url(self, obj, size=settings.AVATAR_DEFAULT_SIZE):
        return u_get_avatar_url(obj, size)

    def validate_avatar(self, avatar):
        return u_validate_avatar(avatar)

    def save(self, *args, **kwargs):
        request = self.context.get('request')
        user = request.user
        if 'avatar' in request.FILES:
            avatar = Avatar(user=user, primary=True)
            image_file = request.FILES['avatar']
            avatar.avatar.save(image_file.name, image_file)
            print(image_file.name)
            print(image_file)
            avatar.save()
            avatar_updated.send(sender=Avatar, user=user, avatar=avatar)
        return user
