from django.urls import path, include
from . import views


urlpatterns = [
    path('api/user/avatar/', views.upload_avatar_view)
]
