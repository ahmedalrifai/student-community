import os
import itertools

from django.contrib.auth.models import User
from django.template.defaultfilters import filesizeformat
from django.utils.module_loading import import_string
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from backend.api.utils import u_get_avatar_url, u_validate_avatar
from avatar.models import Avatar

from django.core import serializers as core_serializers

from . import models


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Message
        fields = ('url', 'subject', 'body', 'pk')


class UniversitySerializer(serializers.ModelSerializer):
    number_of_users = SerializerMethodField()
    admins_info = SerializerMethodField()
    class Meta:
        model = models.University
        fields = (
            'id',
            'name', 
            'created_at', 
            'updated_at', 
            'slug', 
            'found_date', 
            'description', 
            'colleges',
            'number_of_users',
            'admins',
            'admins_info',
            'posts'
        )
        read_only_fields = ('posts', 'colleges', 'slug', 'number_of_users')
    
    def get_number_of_users(self, obj):
        return obj.profiles.count()
    
    def get_admins_info(self, obj):
        admins = obj.admins
        admins_info = []

        for admin in admins.all():
            
            avatar_url = u_get_avatar_url(admin, settings.AVATAR_DEFAULT_SIZE)
            
            admins_info.append({
                'username': admin.username,
                'name': admin.get_full_name(),
                'avatar_url': avatar_url
            })
        
        return admins_info


class CollegeSerializer(serializers.ModelSerializer):
    number_of_users = SerializerMethodField()
    university = SerializerMethodField()
    admins_info = SerializerMethodField()
    university_admins_info = SerializerMethodField()

    class Meta:
        model = models.College
        fields = (
            'id',
            'name',
            'university',
            'created_at', 
            'updated_at', 
            'slug', 
            'found_date', 
            'description',
            'departments',
            'number_of_users',
            'admins',
            'admins_info',
            'university_admins_info',
            'posts'
        )
        read_only_fields = ('posts', 'departments', 'slug', 'university', 'number_of_users')
    
    def get_university(self, obj):
        university = obj.university
        name = university.name
        slug = university.slug

        return {
            'name': name,
            'slug': slug
        }
    
    def get_number_of_users(self, obj):
        return obj.profiles.count()
    
    @staticmethod
    def _get_admins(obj):
        admins = obj.admins
        admins_info = []

        for admin in admins.all():
            avatar_url = u_get_avatar_url(admin, settings.AVATAR_DEFAULT_SIZE)

            admins_info.append({
                'username': admin.username,
                'name': admin.get_full_name(),
                'avatar_url': avatar_url
            })

        return admins_info
    
    def get_admins_info(self, obj):
        return self._get_admins(obj)
    
    def get_university_admins_info(self, obj):
        return self._get_admins(obj.university)

    def create(self, validated_data):
        request = self.context.get('request')
        path_list = request.path.split('/')[1:-1]
        university_slug = path_list[-2]

        university_id = models.University.objects.get(slug=university_slug).id
        
        return models.College.objects.create(
            name=validated_data['name'],
            description=validated_data['description'],
            found_date=validated_data['found_date'],
            university_id=university_id
        )


class DepartmentSerializer(serializers.ModelSerializer):
    university = SerializerMethodField()
    college = SerializerMethodField()
    number_of_users = SerializerMethodField()
    admins_info = SerializerMethodField()
    college_admins_info = SerializerMethodField()
    university_admins_info = SerializerMethodField()

    class Meta:
        model = models.Department
        fields = (
            'id',
            'name',
            'university',
            'college',
            'created_at', 
            'updated_at', 
            'slug', 
            'color',
            'number_of_users',
            'admins',
            'admins_info',
            'college_admins_info',
            'university_admins_info',
            'posts'
        )
        read_only_fields = ('posts', 'slug', 'university', 'college', 'number_of_users')
    
    def get_university(self, obj):
        university = obj.college.university
        name = university.name
        slug = university.slug

        return {
            'name': name,
            'slug': slug
        }
    
    def get_college(self, obj):
        college = obj.college
        name = college.name
        slug = college.slug

        return {
            'name': name,
            'slug': slug
        }

    def get_number_of_users(self, obj):
        return obj.profiles.count()
    
    @staticmethod
    def _get_admins(obj):
        admins = obj.admins
        admins_info = []

        for admin in admins.all():
            avatar_url = u_get_avatar_url(admin, settings.AVATAR_DEFAULT_SIZE)

            admins_info.append({
                'username': admin.username,
                'name': admin.get_full_name(),
                'avatar_url': avatar_url
            })

        return admins_info
    
    def get_admins_info(self, obj):
        return self._get_admins(obj)
    
    def get_college_admins_info(self, obj):
        return self._get_admins(obj.college)
    
    def get_university_admins_info(self, obj):
        return self._get_admins(obj.college.university)
    
    def create(self, validated_data):
        request = self.context.get('request')
        path_list = request.path.split('/')[1:-1]
        college_slug = path_list[-2]

        color = validated_data.get('color')

        college_id = models.College.objects.get(slug=college_slug).id
        if not color:
            raise serializers.ValidationError({"color": ["This field may not be blank."]})
            
        return models.Department.objects.create(
            name=validated_data.get('name'),
            color=color,
            college_id=college_id
        )


class PostSerializer(serializers.ModelSerializer):
    university_info = SerializerMethodField()
    college_info = SerializerMethodField()
    department_info = SerializerMethodField()
    user = SerializerMethodField()
    voters_up = SerializerMethodField()
    voters_down = SerializerMethodField()

    class Meta:
        model = models.Post
        fields = (
            'id',
            'title',
            'slug',
            'content',
            'user',
            'university',
            'university_info',
            'college',
            'college_info',
            'department',
            'department_info',
            'created_at',
            'updated_at',
            'vote_score',
            'num_vote_up',
            'num_vote_down',
            'voters_up',
            'voters_down',
            'comments',
        )
        read_only_fields = (
            'comments',
            'slug',
            'user', 
            'university', 
            'college', 
            'department',
            'vote_score',
            'num_vote_up',
            'num_vote_down',
        )
    
    def get_university_info(self, obj):
        if obj.university is not None:
            slug = obj.university.slug
            name = obj.university.name

            return {
                'slug': slug,
                'name': name
            }
    
    def get_college_info(self, obj):
        if obj.college is not None:
            slug = obj.college.slug
            name = obj.college.name

            return {
                'slug': slug,
                'name': name
            }
    
    def get_department_info(self, obj):
        if obj.department is not None:
            slug = obj.department.slug
            name = obj.department.name
            color = obj.department.color

            return {
                'slug': slug,
                'name': name,
                'color': color
            }
    
    def get_user(self, obj):
        user = obj.user
        username = user.username
        name = user.get_full_name()
        avatar_url = u_get_avatar_url(user, settings.AVATAR_DEFAULT_SIZE)

        return {
            'username': username,
            'name': name,
            "avatar_url": avatar_url
        }

    def get_voters_up(self, obj):
        return obj.votes.user_ids()
    
    def get_voters_down(self, obj):
        return obj.votes.user_ids(action=1)

    def create(self, validated_data):
        request = self.context.get('request')
        path_list = request.path.split('/')[2:-2]
        path_dict = dict(itertools.zip_longest(*[iter(path_list)] * 2, fillvalue=""))
        university_slug = path_dict.get('universities')
        college_slug = path_dict.get('colleges')
        department_slug = path_dict.get('departments')

        university_id = models.University.objects.get(slug=university_slug).id

        if college_slug:
            college_id = models.College.objects.get(slug=college_slug).id
        else:
            college_id = None
        
        if department_slug:
            department_id = models.Department.objects.get(slug=department_slug).id
        else:
            department_id = None

        return models.Post.objects.create(
            title=validated_data['title'],
            content=validated_data['content'],
            user=request.user,
            university_id=university_id,
            college_id=college_id,
            department_id=department_id
        )


class CommentSerializer(serializers.ModelSerializer):
    voters_up = SerializerMethodField()
    voters_down = SerializerMethodField()
    user = SerializerMethodField()

    class Meta:
        model = models.Comment
        fields = (
            'id',
            'content',
            'user',
            'post',
            'created_at',
            'updated_at',
            'parent',
            'vote_score',
            'num_vote_up',
            'num_vote_down',
            'voters_up',
            'voters_down',
            'replies'
        )
        read_only_fields = (
            'replies',
            'user', 
            'post', 
            'parent', 
            'vote_score', 
            'num_vote_up', 
            'num_vote_down',
        )
    
    def get_voters_up(self, obj):
        return obj.votes.user_ids()
    
    def get_voters_down(self, obj):
        return obj.votes.user_ids(action=1)
    
    def get_user(self, obj):
        user = obj.user
        username = user.username
        name = user.get_full_name()
        avatar_url = u_get_avatar_url(user, settings.AVATAR_DEFAULT_SIZE)

        return {
            'username': username,
            'name': name,
            "avatar_url": avatar_url
        }

    def create(self, validated_data):
        request = self.context.get('request')
        path_list = request.path.split('/')[1:-1]
        post_slug = path_list[-2]

        post_id = models.Post.objects.get(slug=post_slug).id

        return models.Comment.objects.create(
            content=validated_data['content'],
            user=request.user,
            post_id=post_id,
            parent=None
        )


class ReplySerializer(serializers.ModelSerializer):
    voters_up = SerializerMethodField()
    voters_down = SerializerMethodField()
    user = SerializerMethodField()

    class Meta:
        model = models.Comment
        fields = (
            'id',
            'content',
            'user',
            'post',
            'created_at',
            'updated_at',
            'parent',
            'vote_score',
            'num_vote_up',
            'num_vote_down',
            'voters_up',
            'voters_down',
        )
        read_only_fields = (
            'replies', 
            'user', 
            'post', 
            'parent',
            'vote_score',
            'num_vote_up',
            'num_vote_down',
        )

    def get_voters_up(self, obj):
        return obj.votes.user_ids()
    
    def get_voters_down(self, obj):
        return obj.votes.user_ids(action=1)
    
    def get_user(self, obj):
        user = obj.user
        username = user.username
        name = user.get_full_name()
        avatar_url = u_get_avatar_url(user, settings.AVATAR_DEFAULT_SIZE)

        return {
            'username': username,
            'name': name,
            "avatar_url": avatar_url
        }

    def create(self, validated_data):
        request = self.context.get('request')
        path_list = request.path.split('/')[1:-1]
        comment_id = path_list[-2]
        post_slug = path_list[-4]

        post_id = models.Post.objects.get(slug=post_slug).id

        return models.Comment.objects.create(
            content=validated_data['content'],
            user=request.user,
            post_id=post_id,
            parent_id=comment_id
        )
