import os

from django.template.defaultfilters import slugify, filesizeformat
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.conf import settings
from django.utils.module_loading import import_string

from rest_framework import serializers


def get_unique_slug(model_instance, slugable_field_name, slug_field_name):
    '''
    Takes a model instance, sluggable field name (such as 'title') of that
    model as string, slug field name (such as 'slug') of the model as string;
    returns a unique slug as string.
    '''
    slug = slugify(getattr(model_instance, slugable_field_name),
                   allow_unicode=True)
    unique_slug = slug
    extension = 1
    ModelClass = model_instance.__class__

    while ModelClass._default_manager.filter(
            **{slug_field_name: unique_slug}
    ).exists():
        unique_slug = f'{slug}-{extension}'
        extension += 1

    return unique_slug


def collage_univarsity_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    model_name_plural = 'colleges'
    if instance.__class__.__name__ == 'University':
        model_name_plural = 'universities'
    category_name = slugify(instance.name, allow_unicode=True)
    return f'images/{model_name_plural}/{category_name}/{filename}'


def u_get_avatar_url(obj, size):
    for provider_path in settings.AVATAR_PROVIDERS:
            provider = import_string(provider_path)
            avatar_url = provider.get_avatar_url(obj, size)
            if avatar_url:
                return avatar_url


def u_validate_avatar(avatar):
        if settings.AVATAR_ALLOWED_FILE_EXTS:
            root, ext = os.path.splitext(avatar.name.lower())
            if ext not in settings.AVATAR_ALLOWED_FILE_EXTS:
                valid_exts = ', '.join(settings.AVATAR_ALLOWED_FILE_EXTS)
                error = _("Can't use %(ext)s .. Valid extensions:%(valid_exts_list)s")
                raise serializers.ValidationError(error %
                                                  {'ext': ext,
                                                   'valid_exts_list': valid_exts})

        if avatar.size > settings.AVATAR_MAX_SIZE:
            error = _("File is too big %(size)s, Maximum size is:%(max_valid_size)s")

            raise serializers.ValidationError(error % {
                'size': filesizeformat(avatar.size),
                'max_valid_size': filesizeformat(settings.AVATAR_MAX_SIZE)
            })
