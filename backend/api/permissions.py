from rest_framework.permissions import BasePermission


class IsOwnerOrReadOnley(BasePermission):
    TARGETED_METHODS = ['PUT', 'DELETE', 'PATCH']
    def has_object_permission(self, request, view, obj):
        if request.method in self.TARGETED_METHODS:
            return request.user == obj.user
        return True

class IsUniversityAdmin(BasePermission):
    TARGETED_METHODS = ['POST', 'PUT', 'DELETE', 'PATCH']
    def has_object_permission(self, request, view, obj):
        print(obj.admins.all())
        if request.method in self.TARGETED_METHODS:
            return request.user in obj.admins.all() or request.user.is_superuser
        return True


class IsCollegeAdmin(BasePermission):
    TARGETED_METHODS = ['POST', 'PUT', 'DELETE', 'PATCH']
    def has_object_permission(self, request, view, obj):
        if request.method in self.TARGETED_METHODS:
            return request.user in obj.admins.all() or request.user in obj.university.admins.all() or request.user.is_superuser
        
        return True


class IsDepratmentAdmin(BasePermission):
    TARGETED_METHODS = ['POST', 'PUT', 'DELETE', 'PATCH']
    def has_object_permission(self, request, view, obj):
        if request.method in self.TARGETED_METHODS:
            return request.user in obj.admins.all() or request.user in obj.college.admins.all() \
                or request.user in obj.college.university.admins.all() or request.user.is_superuser
        return True


class IsPostAdminOrOwnerOrReadOnley(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user == obj.user or request.user.is_superuser:
            return True

        if request.method == 'DELETE':
            return request.user in obj.university.admins.all() or request.user in obj.college.admins.all() \
                or request.user in obj.department.admins.all() 
        
        return True

class IsCommentAdminOrOwnerOrReadOnley(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user == obj.user or request.user.is_superuser:
            return True

        if request.method == 'DELETE':
            return request.user in obj.post.university.admins.all() or request.user in obj.post.college.admins.all() \
                or request.user in obj.post.department.admins.all() or request.user == obj.post.user
        
        return True

class IsReplyAdminOrOwnerOrReadOnley(BasePermission):
    TARGETED_METHODS = ['PUT', 'DELETE', 'PATCH']
    def has_object_permission(self, request, view, obj):
        if request.user == obj.user or request.user.is_superuser:
            return True

        if request.method == 'DELETE':
            return request.user in obj.post.university.admins.all() or request.user in obj.post.college.admins.all() \
                or request.user in obj.post.department.admins.all() or request.user == obj.parent.user or request.user == obj.post.user

        return True
