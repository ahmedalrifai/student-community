# Generated by Django 2.2.1 on 2019-05-07 16:47

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0009_auto_20190220_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='college',
            name='admins',
            field=models.ManyToManyField(blank=True, null=True, related_name='managed_colleges', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='department',
            name='admins',
            field=models.ManyToManyField(blank=True, null=True, related_name='managed_departments', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='university',
            name='admins',
            field=models.ManyToManyField(blank=True, null=True, related_name='managed_universites', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='comment',
            name='level',
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name='comment',
            name='lft',
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name='comment',
            name='rght',
            field=models.PositiveIntegerField(editable=False),
        ),
    ]
