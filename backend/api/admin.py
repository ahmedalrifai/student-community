from django.contrib import admin
from . import models


admin.site.register(models.Post)
admin.site.register(models.University)
admin.site.register(models.College)
admin.site.register(models.Department)
admin.site.register(models.Comment)
