from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView
from django.db.models import Q

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination

from . import models, serializers
from . import permissions
# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = models.Message.objects.all()
    serializer_class = serializers.MessageSerializer


class UniversityViewSet(viewsets.ModelViewSet):
    queryset = models.University.objects.all()
    serializer_class = serializers.UniversitySerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticatedOrReadOnly, permissions.IsUniversityAdmin)


class CollegeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CollegeSerializer
    queryset = models.College.objects.all()
    lookup_field = 'slug'
    permission_classes = (IsAuthenticatedOrReadOnly, permissions.IsCollegeAdmin, )

    def list(self, request, *args, **kwargs):
        university_slug = kwargs.get('university_slug')
        
        university_obj = models.University.objects.get(slug=university_slug)

        self.queryset =  university_obj.colleges.all()

        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)
      

class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = models.Department.objects.all()
    serializer_class = serializers.DepartmentSerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticatedOrReadOnly, permissions.IsDepratmentAdmin)

    def list(self, request, *args, **kwargs):
        university_slug = kwargs.get('university_slug')
        college_slug = kwargs.get('college_slug')
        
        university_obj = models.University.objects.get(slug=university_slug)

        self.queryset =  university_obj.colleges.get(slug=college_slug).departments.all()

        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)


class PostViewSet(viewsets.ModelViewSet):
    queryset = models.Post.objects.all()
    serializer_class = serializers.PostSerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticatedOrReadOnly,  permissions.IsPostAdminOrOwnerOrReadOnley,)


    def list(self, request, *args, **kwargs):
        university_slug = kwargs.get('university_slug')
        college_slug = kwargs.get('college_slug')
        department_slug = kwargs.get('department_slug')

        if department_slug:
            queryset = models.Department.objects.get(slug=department_slug).posts.all()
        elif college_slug:
            queryset = models.College.objects.get(slug=college_slug).posts.all()
        else:
            queryset = models.University.objects.get(slug=university_slug).posts.all()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    @action(detail=True)
    def vote_up(self, request, university_slug=None, college_slug=None, department_slug=None, slug=None):
        models.Post.objects.get(slug=slug).votes.up(request.user.id)
        return HttpResponse()
    
    @action(detail=True)
    def vote_down(self, request, university_slug=None, college_slug=None, department_slug=None, slug=None):
        models.Post.objects.get(slug=slug).votes.down(request.user.id)
        return HttpResponse()

    @action(detail=True)
    def vote_delete(self, request, university_slug=None, college_slug=None, department_slug=None, slug=None):
        models.Post.objects.get(slug=slug).votes.delete(request.user.id)
        return HttpResponse()
        
class CommentViewSet(viewsets.ModelViewSet):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, permissions.IsCommentAdminOrOwnerOrReadOnley)

    def list(self, request, *args, **kwargs):
        print(kwargs)
        post_slug = kwargs.get('post_slug')
        post_id = models.Post.objects.get(slug=post_slug).id

        self.queryset =  models.Post.objects.get(slug=post_slug).comments.filter(parent=None)
        #self.queryset = get_list_or_404(models.Comment, post=post_id, parent=None)

        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)
    
    @action(detail=True)
    def vote_up(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, pk=None):
        models.Comment.objects.get(id=pk).votes.up(request.user.id)
        return HttpResponse()
    
    @action(detail=True)
    def vote_down(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, pk=None):
        models.Comment.objects.get(id=pk).votes.down(request.user.id)
        return HttpResponse()

    @action(detail=True)
    def vote_delete(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, pk=None):
        models.Comment.objects.get(id=pk).votes.delete(request.user.id)
        return HttpResponse()  

class ReplyViewSet(viewsets.ModelViewSet):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.ReplySerializer
    permission_classes = (IsAuthenticatedOrReadOnly, permissions.IsReplyAdminOrOwnerOrReadOnley)

    def list(self, request, *args, **kwargs):
        comment_id = kwargs.get('comment_pk')
        
        self.queryset =  models.Comment.objects.filter(parent=comment_id)

        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)
    
    @action(detail=True)
    def vote_up(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, comment_pk=None, pk=None):
        models.Comment.objects.get(id=pk).votes.up(request.user.id)
        return HttpResponse()
    
    @action(detail=True)
    def vote_down(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, comment_pk=None, pk=None):
        models.Comment.objects.get(id=pk).votes.down(request.user.id)
        return HttpResponse()

    @action(detail=True)
    def vote_delete(self, request, university_slug=None, college_slug=None, department_slug=None, post_slug=None, comment_pk=None, pk=None):
        models.Comment.objects.get(id=pk).votes.delete(request.user.id)
        return HttpResponse()


class NewsFeedListView(ListAPIView):
    queryset = models.Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated,)


    def list(self, request, *args, **kwargs):
        university = request.user.profile.university
        college = request.user.profile.college
        department = request.user.profile.department

        queryset = []

        if university is not None:

            queryset = models.Post.objects.filter(Q(university__id=university.id) & Q(college__id=college.id) & Q(department__id=department.id)
                    | Q(university__id=university.id) & Q(college__id=college.id) & Q(department=None) 
                    | Q(university__id=university.id) & Q(college=None) & Q(department=None) )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
