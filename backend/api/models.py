from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.contrib.auth.models import User

from taggit.managers import TaggableManager

from mptt.models import MPTTModel, TreeForeignKey

from colorfield.fields import ColorField

from vote.models import VoteModel

from .utils import get_unique_slug, collage_univarsity_path


class Message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.TextField()


class BaseModel(models.Model):
    name = models.CharField(max_length=40)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=140, blank=True, allow_unicode=True, verbose_name=_('Slug'))

    def save(self, *args, **kwargs):
        self._create_slug()
        super().save()

    def _create_slug(self):
        if not self.slug:
            self.slug = get_unique_slug(self, 'name', 'slug')

    def __str__(self):
        return f"{self.name}"

    class Meta():
        abstract = True


class Post(VoteModel, models.Model):
    #POST_TYPES = (
    #    (1, _("Topic")),
    #    (2, _("External link"))
    #)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=140, unique=True,
                            blank=True, allow_unicode=True, verbose_name=_('slug'))
    content = models.TextField(blank=True, null=True)
    # post_type = models.IntegerField(choices=TOPIC_TYPES, default=1)
    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    university = models.ForeignKey('University', related_name="posts",
      on_delete=models.CASCADE)
    college = models.ForeignKey('College', related_name="posts",
      blank=True, null=True, on_delete=models.CASCADE)
    department = models.ForeignKey('Department', related_name="posts",
      blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    tages = TaggableManager()
        
    def save(self, *args, **kwargs):
        self.create_slug()
        super().save()

    def create_slug(self):
        if not self.slug:
            self.slug = get_unique_slug(self, 'title', 'slug')

    def __str__(self):
        return f"{self.title}"


class Comment(VoteModel, MPTTModel):
    content = models.TextField()
    user = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='replies')

    def __str__(self):
        return f'Comment {self.pk}'
    
    class MPTTMeta:
        order_insertion_by = ['-created_at']


class University(BaseModel):
    description = models.TextField(blank=True, null=True)
    found_date = models.DateField()
    admins = models.ManyToManyField(User, related_name='managed_universites', blank=True)

    class Meta:
        verbose_name = _("University")
        verbose_name_plural = _("Universities")


class College(BaseModel):
    description = models.TextField(blank=True, null=True)
    found_date = models.DateField()
    university = models.ForeignKey("University", related_name="colleges", verbose_name=_("University"),
      on_delete=models.CASCADE)
    admins = models.ManyToManyField(User, related_name='managed_colleges', blank=True)


class Department(BaseModel):
    college = models.ForeignKey("College", related_name="departments", verbose_name=_("College"),
      on_delete=models.CASCADE)
    color = ColorField(default='#2649cc')
    admins = models.ManyToManyField(User, related_name='managed_departments', blank=True)

